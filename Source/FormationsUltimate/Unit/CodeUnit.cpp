// Fill out your copyright notice in the Description page of Project Settings.

#include "FormationsUltimate.h"
#include "CodeUnit.h"
#include "Unit/AttackStrategies/UnitAttackStrategyBase.h"
#include "Unit/RangeStrategies/UnitRangeStrategyBase.h"
#include "Unit/RecieveDamageStrategies/UnitRecieveDamageStrategy.h"




void UCodeUnit::RecieveDamage(UCodeUnit * Attacker, float damage)
{
	OnRecieveDamage.Broadcast(Attacker, this, damage);
	RecieveDamageStrategy->RecieveDamage(Attacker, this, damage);
}

bool UCodeUnit::CanAttack(UCodeUnit * UnitToBeChecked)
{
	return RangeCheckStrategy->CanAttackUnit(this, UnitToBeChecked);
}

void UCodeUnit::CanBeAttackedBy(UCodeUnit * UnitToBeChecked)
{
	bool canAttack = RangeCheckStrategy->CanAttackUnit(UnitToBeChecked, this);
	IsAttackable = canAttack;
}

void UCodeUnit::Attack(UCodeUnit * UnitToBeAttacked)
{
	OnAttack.Broadcast(this, UnitToBeAttacked);
	AttackStrategy->Attack(this, UnitToBeAttacked);
}

void UCodeUnit::PlaceUnitOnTheBattlefield(int32 XCoord, int32 YCoord)
{
	XCoordinate = XCoord;
	YCoordinate = YCoord;
}

void UCodeUnit::ResetUnit()
{
	IsAttackable = false;
	IsSelected = false;
}

void UCodeUnit::ResetUnitAfterCombat()
{
	XCoordinate = -1;
	YCoordinate = -1;
	Attacks = MaxAttacks;
}

void UCodeUnit::AddToOwner(AArmy * newArmyOwner)
{
	ArmyOwner = newArmyOwner;
}

void UCodeUnit::RemoveFromOwner()
{

}