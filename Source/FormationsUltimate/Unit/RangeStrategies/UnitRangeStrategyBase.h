// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "UnitRangeStrategyBase.generated.h"

class UCodeUnit;
class UCombat;

/**
 * 
 */
UCLASS(Abstract, EditInlineNew, Blueprintable)
class FORMATIONSULTIMATE_API UUnitRangeStrategyBase : public UObject
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "CheckRange")
		bool CanAttackUnit(UCodeUnit * Attacker, UCodeUnit * DefenderToBeChecked);
	UFUNCTION(BlueprintCallable, Category = "CheckRange")
		void Setup(UCombat * combat, UCodeUnit * owner);

	UPROPERTY(BlueprintReadOnly, Category = "Combat")
		UCombat * CurrentCombat;
	UPROPERTY(BlueprintReadOnly, Category = "Combat")
		UCodeUnit * UnitOwner;
};
