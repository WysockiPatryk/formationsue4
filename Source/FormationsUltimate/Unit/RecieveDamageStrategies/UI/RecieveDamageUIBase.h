// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Unit/CodeUnit.h"
#include "RecieveDamageUIBase.generated.h"

/**
 * 
 */
UCLASS()
class FORMATIONSULTIMATE_API URecieveDamageUIBase : public UUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly, Category = "Unit", Meta = (ExposeOnSpawn = true))
		UCodeUnit * Attacker;
	UPROPERTY(BlueprintReadOnly, Category = "Unit", Meta = (ExposeOnSpawn = true))
		UCodeUnit * Defender;
	UPROPERTY(BlueprintReadOnly, Category = "Unit", Meta = (ExposeOnSpawn = true))
		float Damage;
	
	
};
