// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Unit/RecieveDamageStrategies/UI/RecieveDamageUIBase.h"
#include "UnitRecieveDamageStrategy.generated.h"

class UCodeUnit;
class UCombat;

/**
 * 
 */
UCLASS(Abstract, EditInlineNew, Blueprintable)
class FORMATIONSULTIMATE_API UUnitRecieveDamageStrategy : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "RecieveDamage")
		void RecieveDamage(UCodeUnit * Attacker, UCodeUnit * Defender, float damage);
	UFUNCTION(BlueprintCallable, Category = "RecieveDamage")
		void Setup(UCombat * combat, UCodeUnit * owner);

	UPROPERTY(BlueprintReadOnly, Category = "Combat")
		UCombat * CurrentCombat;
	UPROPERTY(BlueprintReadOnly, Category = "Combat")
		UCodeUnit * UnitOwner;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Widget")
		TSubclassOf<URecieveDamageUIBase> DisplayWidget;
};
