// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Unit/RecieveDamageStrategies/UI/RecieveDamageUIBase.h"
#include "UnitAttackStrategyBase.generated.h"

class UCodeUnit;
class UCombat;

/**
 * 
 */
UCLASS(Abstract, EditInlineNew, Blueprintable)
class FORMATIONSULTIMATE_API UUnitAttackStrategyBase : public UObject
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Attack")
		void Attack(UCodeUnit * Attacker, UCodeUnit * Defender);
	UFUNCTION(BlueprintCallable, Category = "Attack")
		void Setup(UCombat * combat, UCodeUnit * owner);

	UPROPERTY(BlueprintReadOnly, Category = "Combat")
		UCombat * CurrentCombat;
	UPROPERTY(BlueprintReadOnly, Category = "Combat")
		UCodeUnit * UnitOwner;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Widget")
		bool isAnEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Widget")
		TSubclassOf<URecieveDamageUIBase> DisplayWidget;
};
