// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "MovementModifier.generated.h"

class AMapNode;

/**
 * 
 */
UCLASS(EditInlineNew)
class FORMATIONSULTIMATE_API UMovementModifier : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, Category = "MovementModifier")
		TSubclassOf<AMapNode> Node;
	UPROPERTY(EditAnywhere, Category = "MovementModifier")
		float NewCost;
	UPROPERTY(EditAnywhere, Category = "MovementModifier")
		float FlatArmyMovementChange;
	
};
