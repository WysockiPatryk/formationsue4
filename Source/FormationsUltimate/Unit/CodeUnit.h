// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "PaperSprite.h"
#include "CodeUnit.generated.h"

class UMovementModifier;
class UUnitRecieveDamageStrategy;
class UUnitAttackStrategyBase;
class UUnitRangeStrategyBase;
class AArmy;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnReceiveDamage, UCodeUnit *, Attacker, UCodeUnit *, Defender, float, damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAttack, UCodeUnit*, UnitToAttack, UCodeUnit *, UnitToBeAttacked);

/**
 * 
 */
UCLASS(Blueprintable, EditInlineNew, Abstract)
class FORMATIONSULTIMATE_API UCodeUnit : public UObject
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stats")
		FText Name;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stats", meta = (MultiLine = "true"))
		FText Description;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stats")
		int32 MaxHp;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stats")
		int32 Hp;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stats")
		int32 Glory;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stats")
		int32 MaxAttacks;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stats")
		int32 Attacks;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stats")
		bool IsFragile;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stats")
		bool IsDead;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Instanced, Category = "Stats")
		UUnitRecieveDamageStrategy * RecieveDamageStrategy;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Instanced, Category = "Stats")
		UUnitRangeStrategyBase * RangeCheckStrategy;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Instanced, Category = "Stats")
		UUnitAttackStrategyBase * AttackStrategy;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Sprite")
		UMaterialInterface* UnitMaterial;
	UPROPERTY(BlueprintReadWrite, Category = "Sprite")
		UMaterialInstanceDynamic* MaterialInstance;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Sprite")
		UTexture* SpecificUnitTypeTexture;
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, BlueprintPure, Category = "Sprite")
		UMaterialInterface* GetUnitMaterial();
	
	UPROPERTY(BlueprintAssignable, Category = "Attack")
		FOnReceiveDamage OnRecieveDamage;
	UPROPERTY(BlueprintAssignable, Category = "Attack")
		FOnAttack OnAttack;

	UFUNCTION(BlueprintCallable, Category = "Action")
		void RecieveDamage(UCodeUnit * Attacker, float damage);
	UFUNCTION(BlueprintCallable, Category = "Action")
		bool CanAttack(UCodeUnit * UnitToBeChecked);
	UFUNCTION(BlueprintCallable, Category = "Action")
		void CanBeAttackedBy(UCodeUnit * UnitToBeChecked);
	UFUNCTION(BlueprintCallable, Category = "Action")
		void Attack(UCodeUnit * UnitToBeAttacked);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Combat")
		int32 XCoordinate;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Combat")
		int32 YCoordinate;
	UFUNCTION(BlueprintCallable, Category = "Combat")
		void PlaceUnitOnTheBattlefield(int32 XCoord, int32 YCoord);

	UPROPERTY(BlueprintReadOnly, Category = "Army")
		AArmy * ArmyOwner;

	UFUNCTION(BlueprintCallable, Category = "Army")
		void AddToOwner(AArmy * newArmyOwner);
	UFUNCTION(BlueprintCallable, Category = "Army")
		void RemoveFromOwner();

	UFUNCTION(BlueprintCallable, Category = "Combat")
		void ResetUnit();
	UFUNCTION(BlueprintCallable, Category = "Combat")
		void ResetUnitAfterCombat();
	UPROPERTY(BlueprintReadOnly, Category = "Combat")
		bool IsAttackable;
	UPROPERTY(BlueprintReadOnly, Category = "Combat")
		bool IsItsTurn;
	UPROPERTY(BlueprintReadOnly, Category = "Combat")
		bool IsSelected;

	UPROPERTY(Instanced, EditDefaultsOnly, Category = "Movement")
		TArray<UMovementModifier * > MovementModifiers;

};
