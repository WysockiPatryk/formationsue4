// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Combat.generated.h"

class AArmy;
class UCodeUnit;
class AGameModeFormationsUltimate;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCombatBegin, AArmy *, AttackingArmy, AArmy *, DefendingArmy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FCombatEnd, AArmy *, AttackingArmy, AArmy *, DefendingArmy, AArmy *, Winner, AArmy *, Loser);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCombatTurn, AArmy *, CurrentArmy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSelection, UCodeUnit *, SelectedUnit);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRangeCheck, UCodeUnit *, AttackedUnitChecked);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FAttack, UCodeUnit *, Attacker, UCodeUnit *, Defender);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCombatStateChange);


/**
 * 
 */
UCLASS(Blueprintable)
class FORMATIONSULTIMATE_API UCombat : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = "CombatEvent")
		FCombatBegin OnCombatBegin;
	UPROPERTY(BlueprintAssignable, Category = "CombatEvent")
		FCombatEnd OnCombatEnd;
	UPROPERTY(BlueprintAssignable, Category = "CombatEvent")
		FCombatTurn OnCombatTurn;
	UPROPERTY(BlueprintAssignable, Category = "CombatEvent")
		FOnCombatStateChange OnPreparationFinished;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "CombatEvent")
		FOnCombatStateChange OnPreparationStarted;
	UPROPERTY(BlueprintAssignable, Category = "CombatEvent")
		FOnCombatStateChange OnCombatFinished;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "CombatEvent")
		FOnCombatStateChange OnCombatStarted;
	UPROPERTY(BlueprintAssignable, Category = "CombatEvent")
		FSelection OnUnitSelected;
	UPROPERTY(BlueprintAssignable, Category = "CombatEvent")
		FRangeCheck OnUnitRangeChecked;
	UPROPERTY(BlueprintAssignable, Category = "CombatEvent")
		FAttack OnUnitAttack;

	UPROPERTY(BlueprintReadOnly, Category = "Armies")
		AArmy * Attacker;
	UPROPERTY(BlueprintReadOnly, Category = "Armies")
		AArmy * Defender;
	UPROPERTY(BlueprintReadOnly, Category = "Armies")
		AArmy * CurrentArmy;
	UPROPERTY(BlueprintReadOnly, Category = "Armies")
		UCodeUnit * SelectedUnit;

	UFUNCTION(BlueprintCallable, Category = "Combat")
		bool DoesStillHaveMoves(AArmy * armyToCheck);
	UFUNCTION(BlueprintCallable, Category = "Combat")
		bool AreAllUnitsPlaced(AArmy * armyToCheck);
	UFUNCTION(BlueprintCallable, Category = "Combat")
		UCodeUnit * FindUnit(AArmy * armyToCheck, int32 X, int32 Y);

	UFUNCTION(BlueprintCallable, Category = "Combat")
		void BeginCombat(AArmy * AttackingArmy, AArmy * DefendingArmy);
	UFUNCTION(BlueprintCallable, Category = "Combat")
		void PassTurn();
	void CheckForDeaths();
	UFUNCTION(BlueprintCallable, Category = "Combat")
		void PassTurnOnPreparation();

	UFUNCTION(BlueprintCallable, Category = "Setup")
		void SetupCombat();

	UFUNCTION(BlueprintCallable, Category = "Combat")
		void SelectUnit(UCodeUnit * UnitToSelect);
	UFUNCTION(BlueprintCallable, Category = "Combat")
		void DeselectUnit();
	UFUNCTION(BlueprintCallable, Category = "Combat")
		void CheckUnitRange(UCodeUnit * UnitToSelect);
	UFUNCTION(BlueprintCallable, Category = "Combat")
		void ResetUnits();
	UFUNCTION(BlueprintCallable, Category = "Combat")
		void AttackUnit(UCodeUnit * UnitToAttack);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Combat")
		bool IsRowEmpty(AArmy * armyToCheck, int32 row);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Combat")
		AArmy * GetOtherArmy(AArmy * armyToCheck);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Units")
		TArray<UCodeUnit *> GetPossibleUnitsToAttack(UCodeUnit* unit);

	UFUNCTION()
		void CalculateWinnerPoints();

	UPROPERTY(BlueprintReadOnly, Category = "GameMode")
		AGameModeFormationsUltimate * gameMode;

	void ResetUnitsStatus();

};
