// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "CombatStatusEnum.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class ECombatStatus : uint8
{
	CS_Attackers		UMETA(DisplayName = "Strategy"),
	CS_Defenders	 	UMETA(DisplayName = "Unit selected")
};