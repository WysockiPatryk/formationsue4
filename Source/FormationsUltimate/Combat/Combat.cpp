// Fill out your copyright notice in the Description page of Project Settings.

#include "FormationsUltimate.h"
#include "Combat.h"
#include "Army/Army.h"
#include "Leader/Leader.h"
#include "Unit/CodeUnit.h"
#include "Unit/AttackStrategies/UnitAttackStrategyBase.h"
#include "Unit/RangeStrategies/UnitRangeStrategyBase.h"
#include "Unit/RecieveDamageStrategies/UnitRecieveDamageStrategy.h"
#include <list>

void UCombat::SetupCombat()
{
	for (auto& unit : Defender->Units) {
		unit->RecieveDamageStrategy->Setup(this, unit);
		unit->RangeCheckStrategy->Setup(this, unit);
		unit->AttackStrategy->Setup(this, unit);
	}
	for (auto& unit : Attacker->Units) {
		unit->RecieveDamageStrategy->Setup(this, unit);
		unit->RangeCheckStrategy->Setup(this, unit);
		unit->AttackStrategy->Setup(this, unit);
	}
}

void UCombat::BeginCombat(AArmy * AttackingArmy, AArmy * DefendingArmy)
{
	Attacker = AttackingArmy;
	Defender = DefendingArmy;
	CurrentArmy = AttackingArmy;
	SetupCombat();
	OnCombatBegin.Broadcast(Attacker, Defender);
	OnCombatTurn.Broadcast(Attacker);
}

bool UCombat::DoesStillHaveMoves(AArmy * armyToCheck)
{
	return armyToCheck->Units.Num() > 0 ? armyToCheck->Units.ContainsByPredicate([](UCodeUnit * Unit) {return Unit->Attacks > 0; }) : false;
}

UCodeUnit * UCombat::FindUnit(AArmy * armyToCheck, int32 X, int32 Y)
{
	UCodeUnit * ReturnVal = nullptr;
	UCodeUnit * FoundUnit = *armyToCheck->Units.FindByPredicate([&](UCodeUnit * unit) { return (unit->XCoordinate == X && unit->YCoordinate == Y); });
	if (FoundUnit != nullptr) {
		ReturnVal = FoundUnit;
	}
	return ReturnVal;
}

void UCombat::PassTurn()
{
	CheckForDeaths();
	AArmy * NextArmy = nullptr;
	if (Attacker == CurrentArmy) {
		if (DoesStillHaveMoves(Defender)) {
			NextArmy = Defender;
		}
		else if (DoesStillHaveMoves(Attacker) && Defender->Units.Num() > 0) {
			NextArmy = Attacker;
		}
	}
	else if (Defender == CurrentArmy) {
		if (DoesStillHaveMoves(Attacker)) {
			NextArmy = Attacker;
		}
		else if (DoesStillHaveMoves(Defender) && Attacker->Units.Num() > 0) {
			NextArmy = Defender;
		}
	}
	if (NextArmy == nullptr) {
		CalculateWinnerPoints();
	}
	else {
		CurrentArmy = NextArmy;
		OnCombatTurn.Broadcast(CurrentArmy);
		ResetUnits();
	}
	
}
void UCombat::PassTurnOnPreparation()
{
	AArmy * NextArmy = CurrentArmy;
	if (AreAllUnitsPlaced(Defender) && AreAllUnitsPlaced(Attacker)) {
		OnPreparationFinished.Broadcast();
		CurrentArmy = Defender;
		PassTurn();
	}
	else {
		if (Attacker == CurrentArmy) {
			if (!AreAllUnitsPlaced(Defender)) {
				NextArmy = Defender;
			}
		}
		else if (Defender == CurrentArmy) {
			if (!AreAllUnitsPlaced(Attacker)) {
				NextArmy = Attacker;
			}
		}
		CurrentArmy = NextArmy;
		OnCombatTurn.Broadcast(CurrentArmy);
	}
}

bool UCombat::AreAllUnitsPlaced(AArmy * armyToCheck)
{
	bool allArePlaced = true;
	for (int i = 0; i < armyToCheck->Units.Num(); i++ ) {
		UCodeUnit * unit = armyToCheck->Units[i];
		if (unit->XCoordinate == -1 && unit->YCoordinate == -1) {
			allArePlaced = false;
		}
	}
	return allArePlaced;
}

void UCombat::SelectUnit(UCodeUnit * UnitToSelect)
{
	SelectedUnit = UnitToSelect;
	CheckUnitRange(SelectedUnit);
	TArray<UCodeUnit *> units = GetPossibleUnitsToAttack(UnitToSelect);
	if (units.Num() == 0) {
		UnitToSelect->Attacks -= 1;
		DeselectUnit();
		PassTurn();
	}
}

void UCombat::CheckUnitRange(UCodeUnit * UnitToAttackOthers)
{
	for (auto& unit : Defender->Units) {
		bool CanAttack = UnitToAttackOthers->CanAttack(unit);
		unit->IsAttackable = CanAttack;
	}
	for (auto& unit : Attacker->Units) {
		bool CanAttack = UnitToAttackOthers->CanAttack(unit);
		unit->IsAttackable = CanAttack;
	}
}

void UCombat::ResetUnits()
{
	for (auto& unit : Attacker->Units) {
		unit->ResetUnit();
	}

	for (auto& unit : Defender->Units) {
		unit->ResetUnit();
	}
}

void UCombat::DeselectUnit()
{
	SelectedUnit = nullptr;
	for (auto& unit : Defender->Units) {
		unit->ResetUnit();
	}
	for (auto& unit : Attacker->Units) {
		unit->ResetUnit();
	}
}

bool UCombat::IsRowEmpty(AArmy * armyToCheck, int32 row)
{
	return !armyToCheck->Units.ContainsByPredicate([&](UCodeUnit * Unit) { return Unit->XCoordinate == row; });
}

AArmy * UCombat::GetOtherArmy(AArmy * armyToCheck)
{
	return armyToCheck == Attacker ? Defender : Attacker;
}

void UCombat::AttackUnit(UCodeUnit * UnitToAttack)
{
	SelectedUnit->AttackStrategy->Attack(SelectedUnit, UnitToAttack);
	DeselectUnit();
	PassTurn();
}

void UCombat::CheckForDeaths()
{
	std::list<UCodeUnit *> deadUnits;
	for (auto& unit : Defender->Units) {
		if (unit->Hp <= 0) {
			deadUnits.push_back(unit);
		}
	}
	for (auto itr = deadUnits.begin(); itr != deadUnits.end(); itr++) {
		Defender->RemoveUnit(*itr);
	}
	deadUnits.clear();
	for (auto& unit : Attacker->Units) {
		if (unit->Hp <= 0) {
			deadUnits.push_back(unit);
		}
	}
	for (auto itr = deadUnits.begin(); itr != deadUnits.end(); itr++) {
		Attacker->RemoveUnit(*itr);
	}
}

void UCombat::CalculateWinnerPoints()
{
	AArmy * Winner = nullptr;
	AArmy * Loser = nullptr;
	if (Attacker->GetGlory() > Defender->GetGlory()) {
		Winner = Attacker;
		Loser = Defender;
	}
	else {
		Winner = Defender;
		Loser = Attacker;
	}
	float gloryGain = Winner->GetGlory() - Loser->GetGlory();
	ULeader * WinnerLeader = Winner->LeaderOwner;
	ResetUnitsStatus();
	OnCombatEnd.Broadcast(Attacker, Defender, Winner, Loser);
	gameMode->OnGameStateChangeDelegate.Broadcast(EMainGameState::MGS_Strategy);
}

void UCombat::ResetUnitsStatus()
{
	for (auto& Unit : Defender->Units) {
		Unit->ResetUnitAfterCombat();
	}
	for (auto& Unit : Attacker->Units) {
		Unit->ResetUnitAfterCombat();
	}
}

TArray<UCodeUnit *> UCombat::GetPossibleUnitsToAttack(UCodeUnit* unit)
{
	TArray<UCodeUnit *> units;
	for (auto& UnitToBeAttacked : Defender->Units) {
		if (unit->RangeCheckStrategy->CanAttackUnit(unit, UnitToBeAttacked)) {
			units.Add(UnitToBeAttacked);
		}
	}
	for (auto& UnitToBeAttacked : Attacker->Units) {
		if (unit->RangeCheckStrategy->CanAttackUnit(unit, UnitToBeAttacked)) {
			units.Add(UnitToBeAttacked);
		}
	}
	return units;
}