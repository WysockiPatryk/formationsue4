// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Leader.generated.h"

class AGameModeFormationsUltimate;
class AArmy;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnArmyOwnershipChange, AArmy *, Army);

/**
 * 
 */
UCLASS(Blueprintable)
class FORMATIONSULTIMATE_API ULeader : public UObject
{
	GENERATED_BODY()
public:
	ULeader();

	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
		void SetupEvents(AGameModeFormationsUltimate * gameMode);
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString LeaderName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString FractionName;

	void ActivateTurn();
	void DeactivateTurn();

	void AddArmy(AArmy * armyToAdd);
	void RemoveArmy(AArmy * armyToRemove);

	UPROPERTY(BlueprintAssignable, Category = "Armies")
		FOnArmyOwnershipChange OnArmyAdd;
	UPROPERTY(BlueprintAssignable, Category = "Armies")
		FOnArmyOwnershipChange OnArmyRemove;

	UFUNCTION()
		void AddArmyDefault(AArmy * armyToAdd);
	UFUNCTION()
		void RemoveArmyDefault(AArmy * armyToRemove);

	void SetColor(struct FLinearColor color);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Color")
		struct FLinearColor GetColor();

	
		float Glory;
	UFUNCTION(BlueprintCallable, Category = "Stats")
		void AddGlory(float GloryToAdd);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Stats")
		float GetPlayerGlory();

	
		float Gold;
	UFUNCTION(BlueprintCallable, Category = "Stats")
		void PayGold(float goldToPay);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Stats")
		float GetCost();
	UFUNCTION(BlueprintCallable, Category = "Stats")
		void RecieveGold(float goldToPay);

	
		float Food;
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Stats")
		float GetFood();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Stats")
		float GetFoodCost();
	UFUNCTION(BlueprintCallable, Category = "Stats")
		void RecieveFood(float foodToRecieve);



protected:
	UPROPERTY(VisibleAnywhere)
		TArray<AArmy*> armies;
	struct FLinearColor PlayerColor;
};
