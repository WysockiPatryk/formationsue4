// Fill out your copyright notice in the Description page of Project Settings.

#include "FormationsUltimate.h"
#include "Leader.h"

ULeader::ULeader()
{
	OnArmyAdd.AddDynamic(this, &ULeader::AddArmyDefault);
	OnArmyRemove.AddDynamic(this, &ULeader::RemoveArmyDefault);
}

void ULeader::ActivateTurn()
{

}

void ULeader::DeactivateTurn()
{

}

void ULeader::AddArmy(AArmy * armyToAdd)
{
	OnArmyAdd.Broadcast(armyToAdd);
}

void ULeader::RemoveArmy(AArmy * armyToRemove)
{
	OnArmyRemove.Broadcast(armyToRemove);
}

void ULeader::AddArmyDefault(AArmy * armyToAdd)
{
	armies.Add(armyToAdd);
}

void ULeader::RemoveArmyDefault(AArmy * armyToRemove)
{
	armies.Remove(armyToRemove);
}

void ULeader::SetColor(struct FLinearColor color)
{
	PlayerColor = color;
}

struct FLinearColor ULeader::GetColor()
{
	return PlayerColor;
}

void ULeader::AddGlory(float GloryToAdd)
{
	Glory += GloryToAdd;
}

float ULeader::GetPlayerGlory()
{
	return Glory;
}

void ULeader::PayGold(float goldToPay)
{
	Gold -= goldToPay;
}

float ULeader::GetCost()
{
	return 0;
}

void ULeader::RecieveGold(float goldToPay)
{
	Gold += goldToPay;
}

float ULeader::GetFood()
{
	return 0;
}

float ULeader::GetFoodCost()
{
	return 0;
}

void ULeader::RecieveFood(float foodToRecieve)
{
	return;
}