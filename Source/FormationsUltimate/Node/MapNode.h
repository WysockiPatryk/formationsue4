// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include <math.h>
#include "Node/NodeMoveStatus.h"
#include "Node/NodeStatus.h"
#include "GameMode/MainGameState.h"
#include "Army/MovementAlgorithmTask.h"
#include "MapNode.generated.h"

class AArmy;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FNodeStateChange, ENodeStatus, NewStatus);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FNodeMoveStateChange, ENodeMoveStatus, NewStatus);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnArmyMovement, AArmy *, EnteringArmy);

UCLASS()
class FORMATIONSULTIMATE_API AMapNode : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditDefaultsOnly)
		int8 NodeSize;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Army")
		AArmy * ArmyOnNode;

	UPROPERTY(BlueprintAssignable, Category = "Node")
		FNodeStateChange OnStateChangeDelegate;
	UPROPERTY(BlueprintAssignable, Category = "Node")
		FNodeMoveStateChange OnMoveStateChangeDelegate;

	UFUNCTION(BlueprintCallable, Category = "Node")
		void Enter(AArmy* enteringArmy);
	UFUNCTION(BlueprintCallable, Category = "Node")
		void Stay(AArmy* stayingArmy);
	UFUNCTION(BlueprintCallable, Category = "Node")
		void Leave(AArmy* leavingArmy);

	UPROPERTY(BlueprintAssignable, Category = "Node")
		FOnArmyMovement OnArmyEnter;
	UPROPERTY(BlueprintAssignable, Category = "Node")
		FOnArmyMovement OnArmyStay;
	UPROPERTY(BlueprintAssignable, Category = "Node")
		FOnArmyMovement OnArmyLeave;
	UPROPERTY(BlueprintAssignable, Category = "Node")
		FOnArmyMovement OnArmyStayEvent;

	UPROPERTY(VisibleInstanceOnly)
		TArray<AMapNode*> Neighbours;

	// Sets default values for this actor's properties
	AMapNode();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual void OnConstruction(const FTransform& Transform) override;

	void UpdateSingleNeighbour(AMapNode * nodeToAdd);
	UFUNCTION()
		void ReactToGameStateChangeToStrategy(EMainGameState NewStatus);
	
	UFUNCTION(BlueprintNativeEvent)
		bool IsEnterable(AArmy * askingArmy);
	UFUNCTION(BlueprintNativeEvent)
		bool IsAttackable(AArmy * askingArmy);
	UFUNCTION(BlueprintNativeEvent)
		bool IsPassable(AArmy * askingArmy);
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void CheckZoneOfControl(AArmy * ArmyToMoveThere);
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void CheckMovementRange(AArmy * ArmyToMoveThere);

	UFUNCTION()
		void StayDefault(AArmy * askingArmy);
	UFUNCTION()
		void EnterDefault(AArmy * askingArmy);
	UFUNCTION()
		void LeaveDefault(AArmy * askingArmy);

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void ProcessPossibleMovement(AArmy * askingArmy);

	void SetMoveLeft(float value);
	void SetPrevNode(AMapNode * value);
	void SetState(ENodeStatus value);
	void SetMoveState(ENodeMoveStatus value);
	void SetVisited(bool value);

	void ResetNodeMovementState();

	void ResetNode();

	void FindNeighbours();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Movement")
		float GetCost(AArmy * ArmyToMoveThere);
	float GetMoveLeft();
	AMapNode * GetPrevNode();
	bool GetWasVisited();

	UPROPERTY(BlueprintReadOnly, Category = "Movement")
		float CurrentMoveCost;
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		TArray<FString> MovementModifiersTags;

	ENodeStatus GetNodeStatus();
	ENodeMoveStatus GetNodeMovementStatus();

	//Keeps track of armies that have that node in its zone of control
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		TArray<AArmy*> ArmiesInfuencing;
	//Functions for when some army will get into zone of control of this node, or lose it
	void GetIntoZoneOfControlOf(AArmy * ArmyToInfluence);
	void GetOutFromZoneOfControlOf(AArmy * ArmyToLoseInfluence);

	//Keeps track of armies that have this Node in range of movement
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		TArray<AArmy*> ArmiesInMovementRange;
	//Functions for when some army will get into zone of control of this node, or lose it
	void GetIntoMovementRangeOf(AArmy * ArmyToInfluence);
	void GetOutOfMovementRangeOf(AArmy * ArmyToLoseInfluence);

	void CheckForZoneOfControl(TArray<AMapNode*> * CheckedNodes, float MovementLeft, AArmy* ArmyToCheck);

protected:
	void FixPosition(FVector position);
	UPROPERTY(EditAnywhere, Category = "Movement")
		float Cost;
	UPROPERTY(BlueprintReadOnly, Category = "Movement")
		float MoveLeft;
	
	UPROPERTY(BlueprintReadOnly, Category = "Movement")
		AMapNode * PrevNode;
	UPROPERTY(BlueprintReadOnly, Category = "Movement")
		ENodeStatus CurrentNodeStatus;
	UPROPERTY(BlueprintReadOnly, Category = "Movement")
		ENodeMoveStatus CurrentNodeMoveStatus;
	bool WasVisited;
};
