// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
UENUM(BlueprintType)
enum class ENodeMoveStatus : uint8
{
	NMS_Nothing			UMETA(DisplayName = "Nothing"),
	NMS_Route			UMETA(DisplayName = "Route"),
	NMS_Finish	 		UMETA(DisplayName = "Final"),
	NMS_FinishCombat	UMETA(DisplayName = "FinalBeforeCombat")
};

