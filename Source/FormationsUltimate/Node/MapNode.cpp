// Fill out your copyright notice in the Description page of Project Settings.

#include "FormationsUltimate.h"
#include "MapNode.h"
#include "Unit/MovementModifier/MovementModifier.h"
#include "Army/Army.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values
AMapNode::AMapNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	NodeSize = 1;
}

// Called when the game starts or when spawned
void AMapNode::BeginPlay()
{
	Super::BeginPlay();
	OnArmyStayEvent.AddDynamic(this, &AMapNode::StayDefault);
	OnArmyLeave.AddDynamic(this, &AMapNode::LeaveDefault);
	OnArmyEnter.AddDynamic(this, &AMapNode::EnterDefault);

	AGameMode *mode = GetWorld()->GetAuthGameMode();
	AGameModeFormationsUltimate * gameMode = Cast<AGameModeFormationsUltimate>(mode);
	if (gameMode) {
		gameMode->OnGameStateChangeDelegate.AddDynamic(this, &AMapNode::ReactToGameStateChangeToStrategy);
	}
}

// Called every frame
void AMapNode::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AMapNode::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	//FixPosition(this->GetActorLocation());
	for (TObjectIterator<AMapNode> Itr; Itr; ++Itr)
	{
		Itr->FindNeighbours();
	}
}

void AMapNode::UpdateSingleNeighbour(AMapNode * nodeToAdd)
{
	if (!Neighbours.Contains(nodeToAdd) && this != nodeToAdd)
	{
		Neighbours.Add(nodeToAdd);
	}
}

bool AMapNode::IsEnterable_Implementation(AArmy * askingArmy)
{
	return ArmyOnNode == nullptr;
}

bool AMapNode::IsAttackable_Implementation(AArmy * askingArmy)
{
	return false;
}

bool AMapNode::IsPassable_Implementation(AArmy * askingArmy)
{
	return false;
}

void AMapNode::ProcessPossibleMovement(AArmy * askingArmy)
{

}

void AMapNode::StayDefault(AArmy * enteringArmy)
{
	CheckZoneOfControl(enteringArmy);
	CheckMovementRange(enteringArmy);
}

void AMapNode::CheckZoneOfControl(AArmy * ArmyToMoveThere)
{
	MovementAlgorithmTask Task(this, ArmyToMoveThere);
	GridGenerationResult* GenerationResult = Task.GenerateGrid(ArmyToMoveThere->GetMovement());
	for (NodePathfindingStatus * Result : GenerationResult->Results) {
		Result->Node->GetIntoZoneOfControlOf(ArmyToMoveThere);
	}
	delete GenerationResult;
}

void AMapNode::CheckMovementRange(AArmy * ArmyToMoveThere)
{
	MovementAlgorithmTask Task(this, ArmyToMoveThere);
	GridGenerationResult* GenerationResult = Task.GenerateGrid(ArmyToMoveThere->GetMovement());
	for (NodePathfindingStatus * Result : GenerationResult->Results) {
		Result->Node->GetIntoMovementRangeOf(ArmyToMoveThere);
	}
	delete GenerationResult;
}

void AMapNode::EnterDefault(AArmy * askingArmy)
{
	ArmyOnNode = askingArmy;
	ArmyOnNode->Position = this;
}

void AMapNode::LeaveDefault(AArmy * askingArmy)
{
	askingArmy->Position = nullptr;
	ArmyOnNode = nullptr;
}

void AMapNode::Stay(AArmy * enteringArmy)
{
	ArmyOnNode = enteringArmy;
	ArmyOnNode->Position = this;
	OnArmyStayEvent.Broadcast(enteringArmy);
}
void AMapNode::Enter(AArmy* enteringArmy)
{
	OnArmyEnter.Broadcast(enteringArmy);
}

void AMapNode::Leave(AArmy* leavingArmy)
{
	OnArmyLeave.Broadcast(leavingArmy);
}

void AMapNode::FixPosition(FVector position)
{
	float Xval = (int)position.X % NodeSize;
	if (Xval > NodeSize) {
		position.X -= Xval;
		position.X += NodeSize;
	}
	else {
		position.X -= Xval;
	}

	float Yval = (int)position.Y % NodeSize;
	if (Yval > NodeSize) {
		position.Y -= Xval;
		position.Y += NodeSize;
	}
	else {
		position.Y -= Xval;
	}
	this->SetActorLocation(position);
}

void AMapNode::SetMoveLeft(float value)
{
	MoveLeft = value;
	CurrentMoveCost = value;
}

void AMapNode::SetPrevNode(AMapNode * value)
{
	PrevNode = value;
}

void AMapNode::SetState(ENodeStatus value)
{
	if (value != CurrentNodeStatus) {
		OnStateChangeDelegate.Broadcast(value);
		CurrentNodeStatus = value;
	}
}

void AMapNode::SetMoveState(ENodeMoveStatus value)
{
	if (value != CurrentNodeMoveStatus) {
		OnMoveStateChangeDelegate.Broadcast(value);
		CurrentNodeMoveStatus = value;
	}
}

void AMapNode::SetVisited(bool value)
{
	WasVisited = value;
}

void AMapNode::ResetNode()
{
	SetMoveLeft(-1);
	SetPrevNode(nullptr);
	SetState(ENodeStatus::NS_Nothing);
	SetMoveState(ENodeMoveStatus::NMS_Nothing);
	SetVisited(false);
}

void AMapNode::ResetNodeMovementState()
{
	SetMoveState(ENodeMoveStatus::NMS_Nothing);
}

void AMapNode::FindNeighbours()
{
	int directions[4][2] = { { 1, 0 },{ 0, 1 },{ -1, 0 },{ 0, -1 } };

	float Offset = 50.0f;

	TArray<AMapNode*> foundNeighbours;
	AActor* MyOwnerActor = Cast<AMapNode>(GetOuter());

	FVector Start;
	Start = GetActorLocation();

	FVector End;

	float Radius = 10.0f;
	float HalfHeight = 10.0f;

	TArray<TEnumAsByte<enum EObjectTypeQuery> > arrayTraceTypeAsByte;
	arrayTraceTypeAsByte.Add(EObjectTypeQuery::ObjectTypeQuery7);

	bool traceComplex = false;

	TArray<AActor*> IgnoredActors;

	EDrawDebugTrace::Type debugType = EDrawDebugTrace::None;

	TArray <FHitResult> hitResults;

	bool ignoreSelf = true;

	for (int i = 0; i < 4; i++)
	{
		End = FVector(Start.X + (Offset * directions[i][0]), Start.Y + (Offset * directions[i][1]), Start.Z);
		hitResults.Empty();

		UKismetSystemLibrary::CapsuleTraceMultiForObjects(
			GetWorld(),
			Start,
			End,
			Radius,
			HalfHeight,
			arrayTraceTypeAsByte,
			traceComplex,
			IgnoredActors,
			debugType,
			hitResults,
			ignoreSelf
			);
		for (int i = 0; i < hitResults.Num(); i++)
		{
			AMapNode* hit = Cast<AMapNode>(hitResults[i].GetActor());
			if (hit)
			{
				if (!foundNeighbours.Contains(hit) && this != hit)
				{
					foundNeighbours.Add(hit);
					hit->UpdateSingleNeighbour(this);
				}
			}
		}
	}
	Neighbours = foundNeighbours;
}

ENodeStatus AMapNode::GetNodeStatus()
{
	return CurrentNodeStatus;
}
ENodeMoveStatus AMapNode::GetNodeMovementStatus()
{
	return CurrentNodeMoveStatus;
}
float AMapNode::GetCost(AArmy * ArmyToMoveThere)
{
	float tempMoveCost = Cost;
	TArray <UMovementModifier *> FoundModifiers = ArmyToMoveThere->GetMovementModifiers();

	UClass * nodeClass = GetClass();

	TArray <UMovementModifier *> FilteredModifiers = FoundModifiers.FilterByPredicate([&](UMovementModifier * ArmyModifier) { 
		return nodeClass->IsChildOf(*(ArmyModifier->Node));
	});
	for (int i = 0; i < FilteredModifiers.Num(); i++) {
		UMovementModifier * modifier = FilteredModifiers[i];
		if (modifier->NewCost != 0) {
			tempMoveCost = modifier->NewCost;
		}
	}
	CurrentMoveCost = tempMoveCost;
	return tempMoveCost;
}

float AMapNode::GetMoveLeft()
{
	return this->MoveLeft;
}

AMapNode * AMapNode::GetPrevNode()
{
	return this->PrevNode;
}

bool AMapNode::GetWasVisited()
{
	return this->WasVisited;
}

void AMapNode::ReactToGameStateChangeToStrategy(EMainGameState NewStatus)
{
	if (NewStatus == EMainGameState::MGS_Strategy) {
		ResetNode();
	}
}

void AMapNode::CheckForZoneOfControl(TArray<AMapNode*> * CheckedNodes, float MovementLeft, AArmy* ArmyToCheck)
{
	CheckedNodes->Add(this);
	float Cost = GetCost(ArmyToCheck);
	if (MovementLeft >= Cost) {
		GetIntoZoneOfControlOf(ArmyToCheck);
		for (AMapNode* Neighbour : Neighbours)
		{
			if (!CheckedNodes->Contains(Neighbour)) {
				Neighbour->CheckForZoneOfControl(CheckedNodes, MovementLeft - Cost, ArmyToCheck);
			}
		}
	}
	else {
		return;
	}
}

void AMapNode::GetIntoZoneOfControlOf(AArmy * ArmyToInfluence)
{
	ArmiesInfuencing.Add(ArmyToInfluence);
	ArmyToInfluence->NodesInZoneOfControl.Add(this);
}

void AMapNode::GetOutFromZoneOfControlOf(AArmy * ArmyToLoseInfluence)
{
	ArmiesInfuencing.Remove(ArmyToLoseInfluence);
	ArmyToLoseInfluence->NodesInZoneOfControl.Remove(this);
}

void AMapNode::GetIntoMovementRangeOf(AArmy * ArmyToInfluence)
{
	ArmiesInMovementRange.Add(ArmyToInfluence);
	ArmyToInfluence->NodesInRange.Add(this);
}

void AMapNode::GetOutOfMovementRangeOf(AArmy * ArmyToLoseInfluence)
{
	ArmiesInMovementRange.Remove(ArmyToLoseInfluence);
	ArmyToLoseInfluence->NodesInRange.Remove(this);
}