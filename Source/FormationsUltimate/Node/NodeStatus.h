// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
UENUM(BlueprintType)
enum class ENodeStatus : uint8
{
	NS_Nothing 		UMETA(DisplayName = "Nothing"),
	NS_Enterable 	UMETA(DisplayName = "Enterable"),
	NS_Passable		UMETA(DisplayName = "Passable"),
	NS_Attackable	UMETA(DisplayName = "Attackable")
};
