// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MainGameState.generated.h"

/**
 * 
 */
UENUM(BlueprintType)		
enum class EMainGameState : uint8
{
	MGS_Strategy 		UMETA(DisplayName = "Strategy"),
	MGS_UnitSelected 	UMETA(DisplayName = "Unit selected"),
	MGS_UnitMoving		UMETA(DisplayName = "Unit moving"),
	MGS_CombatBegin 	UMETA(DisplayName = "Combat beggining"),
	MGS_Combat		 	UMETA(DisplayName = "Combat loop"),
	MGS_CombatEnd	 	UMETA(DisplayName = "Combat ending")
};
