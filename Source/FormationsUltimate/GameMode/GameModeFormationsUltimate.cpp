// Fill out your copyright notice in the Description page of Project Settings.

#include "FormationsUltimate.h"
#include "GameModeFormationsUltimate.h"
#include "Leader/Leader.h"
#include "Army/Army.h"
#include "Army/MovementAlgorithm.h"
#include "Node/MapNode.h"
#include "Node/NodeMoveStatus.h"
#include "Node/NodeStatus.h"
#include "GameMode/MainGameState.h"
#include "Combat/Combat.h"




void AGameModeFormationsUltimate::SelectArmy(AArmy * NewSelectedArmy)
{
	if (CurrentGameState == EMainGameState::MGS_Strategy || CurrentGameState == EMainGameState::MGS_UnitSelected) {
		if (SelectedArmy != nullptr) {
			SelectedArmy->Deselect();
			OnArmyDeselection.Broadcast(SelectedArmy);
			SelectedArmy = nullptr;
			ChangeGameState(EMainGameState::MGS_Strategy);
		}
		if (NewSelectedArmy != nullptr) {
			SelectedArmy = NewSelectedArmy;
			if (HoveredArmy) {
				OnArmyHoverOff.Broadcast(HoveredArmy);
				HoveredArmy = nullptr;
			}
			ChangeGameState(EMainGameState::MGS_UnitSelected);
			SelectedArmy->Select();
			OnArmySelection.Broadcast(SelectedArmy);
		}
	}
}

AArmy * AGameModeFormationsUltimate::GetSelectedArmy()
{
	return SelectedArmy;
}

ULeader * AGameModeFormationsUltimate::GetLeader(int8 player)
{
	if (player >= Leaders.Num())
		return nullptr;
	else
		return Leaders[player];
}

void AGameModeFormationsUltimate::BeginPlay()
{
	Super::BeginPlay();
	FOnNextTurn.Broadcast(CurrentPlayer, CurrentTurn % 2, CurrentTurn / 2);
}

void AGameModeFormationsUltimate::NotifyTurn()
{
	FOnNextTurn.Broadcast(CurrentPlayer, CurrentTurn % 2, CurrentTurn / 2);
}

void AGameModeFormationsUltimate::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	CombatInstance = NewObject<UCombat>(this, UCombat::StaticClass());
	CombatInstance->gameMode = this;
	CurrentTurn = -1;
	Super::InitGame(MapName, Options, ErrorMessage);
	for (int8 i = 0; i < LeaderTemplates.Num(); i++) {
		ULeader * newLeader = NewObject<ULeader>(this, *LeaderTemplates[i]);
		Leaders.Add(newLeader);
		newLeader->SetColor(Colors[i]);
		newLeader->SetupEvents(this);
		UE_LOG(LogTemp, Warning, TEXT("Created new leader named %s"), *Leaders[i]->LeaderName);
	}
	UE_LOG(LogTemp, Warning, TEXT("Init game"));
	CurrentPlayer = Leaders[Leaders.Num() - 1];
	PassTurn();
	OnGameStateChangeDelegate.AddDynamic(this, &AGameModeFormationsUltimate::OnChangeToStrategy);
	OnPointChange.AddDynamic(this, &AGameModeFormationsUltimate::GivePointsToLeader);
}

int8 AGameModeFormationsUltimate::GetTurnNumber()
{
	return TurnNumber;
}

EMainGameState AGameModeFormationsUltimate::GetCurrentGameState()
{
	return CurrentGameState;
}

void AGameModeFormationsUltimate::ChangeGameState(EMainGameState newState)
{
	if(CurrentGameState != newState)
	{
		CurrentGameState = newState;
		OnGameStateChangeDelegate.Broadcast(newState);
	}
}

void AGameModeFormationsUltimate::SelectNode(AMapNode * NewSelectedNode)
{
	if (CurrentGameState == EMainGameState::MGS_UnitSelected) {
		TargetedArmy = nullptr;
		if (NewSelectedNode->GetNodeMovementStatus() == ENodeMoveStatus::NMS_Finish)
		{
			GotoNode(NewSelectedNode);
		}
		else {
			TargetNode(NewSelectedNode);
		}
	}
}

void AGameModeFormationsUltimate::GotoNode(AMapNode * TargetedNode) {
	if (SelectedArmy)
	{
		TArray<AMapNode*> route = SelectedArmy->Algorithm->GenerateRoute(SelectedArmy->GetPosition(), TargetedNode);
		route.RemoveAt(0);
		SelectedArmy->MoveTo(route);
		ChangeGameState(EMainGameState::MGS_UnitMoving);
		SetGameStateAfterMovement(EMainGameState::MGS_Strategy);
	}
}

void AGameModeFormationsUltimate::TargetNode(AMapNode * TargetedNode) {
	if (TargetedNode->GetNodeStatus() == ENodeStatus::NS_Enterable && CurrentGameState == EMainGameState::MGS_UnitSelected) {
		ResetMovementNodes();
		SelectedNode = TargetedNode;
		if (SelectedArmy) {
			TArray<AMapNode*> route = SelectedArmy->Algorithm->GenerateRoute(SelectedArmy->GetPosition(), TargetedNode);
			for (int i = 0; i < route.Num(); i++) {
				AMapNode * node = route[i];
				node->SetMoveState(ENodeMoveStatus::NMS_Route);
			}
			TargetedNode->SetMoveState(ENodeMoveStatus::NMS_Finish);
		}
	}
}

AMapNode * AGameModeFormationsUltimate::GetSelectedNode()
{
	return SelectedNode;
}

void AGameModeFormationsUltimate::ResetMovementNodes()
{
	for (TActorIterator<AMapNode> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		ActorItr->ResetNodeMovementState();
	}
}

void AGameModeFormationsUltimate::OnChangeToStrategy(EMainGameState NewStatus)
{
	if (NewStatus == EMainGameState::MGS_Strategy) {
		SelectArmy(nullptr);
	}
}

UCombat * AGameModeFormationsUltimate::GetCombat()
{
	return CombatInstance;
}

void AGameModeFormationsUltimate::TargetArmy(AArmy * ArmyToTarget)
{
	if (CurrentGameState != EMainGameState::MGS_UnitSelected || !SelectedArmy || !SelectedArmy->CanAttack) {
		return;
	}
	if (TargetedArmy != nullptr) {
		AttackArmy(ArmyToTarget);
	}
	else if (ArmyToTarget->Position->GetNodeStatus() == ENodeStatus::NS_Attackable) {
		TargetedArmy = ArmyToTarget;
		ResetMovementNodes();
		SelectedNode = ArmyToTarget->Position;
		if (SelectedArmy) {
			TArray<AMapNode*> route = SelectedArmy->Algorithm->GenerateRoute(SelectedArmy->GetPosition(), SelectedNode);
			for (int i = 0; i < route.Num(); i++) {
				AMapNode * node = route[i];
				node->SetMoveState(ENodeMoveStatus::NMS_Route);
			}
			route[route.Num() - 2]->SetMoveState(ENodeMoveStatus::NMS_FinishCombat);
		}
	}
}

void AGameModeFormationsUltimate::AttackArmy(AArmy * ArmyToAttack)
{
	if (ArmyToAttack != SelectedArmy) {
		TArray<AMapNode*> route = SelectedArmy->Algorithm->GenerateRoute(SelectedArmy->GetPosition(), ArmyToAttack->GetPosition());
		route.RemoveAt(route.Num() - 1);
		SelectedArmy->AttackTo(route, ArmyToAttack);
		GetCombat()->BeginCombat(SelectedArmy, ArmyToAttack);
		ChangeGameState(EMainGameState::MGS_UnitMoving);
		SetGameStateAfterMovement(EMainGameState::MGS_CombatBegin);
	}
}

void AGameModeFormationsUltimate::SetGameStateAfterMovement(EMainGameState afterMovement)
{
	AfterMovementState = afterMovement;
}

void AGameModeFormationsUltimate::FinishMovement()
{
	ChangeGameState(AfterMovementState);
}

ULeader * AGameModeFormationsUltimate::GetCurrentLeader()
{
	return CurrentPlayer;
}

void AGameModeFormationsUltimate::PassTurn()
{
	CurrentGameState = EMainGameState::MGS_Strategy;
	OnGameStateChangeDelegate.Broadcast(EMainGameState::MGS_Strategy);
	CurrentTurn += 1;
	CurrentPlayer = Leaders[CurrentTurn % 2];
	FOnNextTurn.Broadcast(CurrentPlayer, CurrentTurn % 2, CurrentTurn / 2);
}

void AGameModeFormationsUltimate::NotifyOnPointChange(FPointChangeStruct PointChange)
{
	OnPointChange.Broadcast(PointChange);
}

void AGameModeFormationsUltimate::GivePointsToLeader(FPointChangeStruct PointChange)
{
	PointChange.LeaderToAlter->AddGlory(PointChange.PointChange);
}

void AGameModeFormationsUltimate::HoverOnArmy(AArmy * NewHoveredArmy)
{
	if (SelectedArmy == nullptr) {
		HoveredArmy = NewHoveredArmy;
		OnArmyHoverOn.Broadcast(HoveredArmy);
	}
}

void AGameModeFormationsUltimate::EndHoverOnArmy(AArmy * EndHoveredArmy)
{
	if (SelectedArmy == nullptr) {
		HoveredArmy = nullptr;
		OnArmyHoverOff.Broadcast(HoveredArmy);
	}
}