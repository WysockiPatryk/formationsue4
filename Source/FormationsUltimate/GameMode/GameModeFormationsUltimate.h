// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "GameMode/MainGameState.h"
#include "GameMode/PointChangeStruct.h"
#include "GameModeFormationsUltimate.generated.h"

class ULeader;
class AMapNode;
class AArmy;
class UCombat;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGameStateChange, EMainGameState, NewStatus);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FArmySelection, AArmy *, SelectedArmy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FTurnPass, ULeader *, CurrentLeader, int32, TurnNumber, int32, TotalTurnNumber);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FTargetCapture, AMapNode *, NodeCaptured, AArmy*, ArmyCapturing, ULeader*, PreviousOwner);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPointChange, FPointChangeStruct, PointChangeStruct);


/**
*
*/
UCLASS()
class FORMATIONSULTIMATE_API AGameModeFormationsUltimate : public AGameMode
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "GameState")
		FGameStateChange OnGameStateChangeDelegate;
	UFUNCTION()
		void OnChangeToStrategy(EMainGameState NewStatus);

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Leaders")
		TArray<TSubclassOf<class ULeader>> LeaderTemplates;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Leaders")
		TArray<class ULeader*> Leaders;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Leaders")
		TArray<struct FLinearColor> Colors;
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Leaders")
		ULeader * GetCurrentLeader();

	UPROPERTY(VisibleAnywhere)
		FString charName;

	UFUNCTION(BlueprintCallable, Category = "Selection")
		void SelectArmy(AArmy * NewSelectedArmy);
	UFUNCTION(BlueprintCallable, Category = "Selection")
		void HoverOnArmy(AArmy * NewSelectedArmy);
	UFUNCTION(BlueprintCallable, Category = "Selection")
		void EndHoverOnArmy(AArmy * NewSelectedArmy);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Selection")
		AArmy * GetSelectedArmy();

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void SelectNode(AMapNode * NewSelectedNode);
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void TargetNode(AMapNode * TargetedNode);
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void GotoNode(AMapNode * TargetedNode);
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void TargetArmy(AArmy * ArmyToTarget);
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void AttackArmy(AArmy * ArmyToAttack);
	UFUNCTION(BlueprintCallable, Category = "Movement")
		AMapNode * GetSelectedNode();

	ULeader * GetLeader(int8);

	virtual void BeginPlay();
	virtual void InitGame(
		const FString & MapName,
		const FString & Options,
		FString & ErrorMessage
		);

	int8 GetTurnNumber();
	EMainGameState GetCurrentGameState();
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void ChangeGameState(EMainGameState newState);
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void SetGameStateAfterMovement(EMainGameState afterMovement);
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void FinishMovement();

	UPROPERTY(BlueprintAssignable, Category = "Selection")
		FArmySelection OnArmySelection;
	UPROPERTY(BlueprintAssignable, Category = "Selection")
		FArmySelection OnArmyDeselection;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Selection")
		FArmySelection OnArmyHoverOn;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Selection")
		FArmySelection OnArmyHoverOff;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Capture")
		FTargetCapture OnNodeCapture;
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Capture")
		FTargetCapture OnNodeLose;

	UFUNCTION(BlueprintCallable, Category = "Points")
		void NotifyOnPointChange(FPointChangeStruct PointChange);
	UFUNCTION()
		void GivePointsToLeader(FPointChangeStruct PointChange);
	UPROPERTY(BlueprintAssignable, Category = "Capture")
		FPointChange OnPointChange;
	

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Combat")
		UCombat * GetCombat();
	
	UPROPERTY(BlueprintAssignable, Category = "Turn")
		FTurnPass FOnNextTurn;
	UFUNCTION(BlueprintCallable, Category = "Turn")
		void PassTurn();
	UFUNCTION(BlueprintCallable, Category = "Turn")
		void NotifyTurn();

private:
	int8 TurnNumber;
	UPROPERTY(EditAnywhere)
		EMainGameState CurrentGameState;
	EMainGameState AfterMovementState;
	AArmy * SelectedArmy;
	AArmy * HoveredArmy;
	AArmy * TargetedArmy;
	AMapNode * SelectedNode;
	void ResetMovementNodes();
	UPROPERTY()
		UCombat * CombatInstance;
	ULeader * CurrentPlayer;
	int32 CurrentTurn;
};