// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PointChangeStruct.generated.h"

class ULeader;

/**
 * 
 */
USTRUCT(BlueprintType)
struct FORMATIONSULTIMATE_API FPointChangeStruct
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		ULeader * LeaderToAlter;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		UTexture * TextureToDisplay;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		FString EventName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float PointChange;

	FPointChangeStruct();
	~FPointChangeStruct();
};
