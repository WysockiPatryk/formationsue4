// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameMode/GameModeFormationsUltimate.h"
#include "GameMode/MainGameState.h"
#include "GameModeInteractionFunctions.generated.h"

/**
 * 
 */
UCLASS()
class FORMATIONSULTIMATE_API UGameModeInteractionFunctions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
