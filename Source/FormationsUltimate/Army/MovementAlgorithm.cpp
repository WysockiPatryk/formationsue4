// Fill out your copyright notice in the Description page of Project Settings.

#include "FormationsUltimate.h"
#include "Node/MapNode.h"
#include "Army/Army.h"
#include "Algo/Reverse.h"
#include "MovementAlgorithm.h"


// Sets default values for this component's properties
UMovementAlgorithm::UMovementAlgorithm()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UMovementAlgorithm::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UMovementAlgorithm::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}

void UMovementAlgorithm::GenerateGrid(AArmy * army)
{
	RemoveGrid(nullptr);
	float movement = army->GetMovement();
	AMapNode * start = army->GetPosition();
	if (!start || movement < 0)
		return;

	TArray<AMapNode*> NodesToTest;
	NodesToTest.Add(start);

	start->SetState(ENodeStatus::NS_Passable);
	start->SetMoveLeft(movement);
	start->SetPrevNode(nullptr);
	start->SetVisited(true);
	
	while (NodesToTest.Num() > 0)
	{
		AMapNode* testedNode = NodesToTest[0];
		for (int i = 0; i < testedNode->Neighbours.Num(); i++)
		{
			AMapNode* nextNode = TryNode(testedNode, testedNode->Neighbours[i], testedNode->GetMoveLeft());
			if (nextNode != nullptr)
			{
				NodesToTest.Add(nextNode);
				nextNode->SetVisited(true);
			}
		}
		NodesToTest.RemoveAt(0);
	}
}

void UMovementAlgorithm::RemoveGrid(AArmy * army)
{
	for (TActorIterator<AMapNode> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		ActorItr->ResetNode();
	}
}

TArray<AMapNode*> UMovementAlgorithm::GenerateRoute(AMapNode* start, AMapNode* end)
{
	TArray<AMapNode*> array;
	AMapNode * PrevNode = end;
	do {
		array.Add(PrevNode);
		PrevNode = PrevNode->GetPrevNode();
	} while (PrevNode != nullptr);
	Algo::Reverse(array);
	return array;
}

AMapNode * UMovementAlgorithm::TryNode(AMapNode* Start, AMapNode* End, float movement)
{
	AMapNode * FoundMap = nullptr;
	bool foundBetter = false;

	if (!IsFaster(Start, End, movement)) {
		return nullptr;
	}
	if (End->IsAttackable(owner)) {
		End->SetState(ENodeStatus::NS_Attackable);
	}
	else if (End->IsEnterable(owner)) {
		End->SetState(ENodeStatus::NS_Enterable);
		FoundMap = End;
	}
	else if (End->IsPassable(owner)) {
		End->SetState(ENodeStatus::NS_Passable);
		FoundMap = End;
	}
	if (IsFaster(Start, End, movement)) {
		End->SetMoveLeft(movement - End->GetCost(owner));
		End->SetPrevNode(Start);
	}
	if (End->GetWasVisited()) {
		return nullptr;
	}
	return FoundMap;
}

bool UMovementAlgorithm::IsFaster(AMapNode* Start, AMapNode* End, float movement)
{
	return (End->GetCost(owner) <= movement) && (End->GetMoveLeft() < (movement - End->GetCost(owner)));
}

