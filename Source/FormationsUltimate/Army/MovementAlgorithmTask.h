// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Army/Army.h"
#include "Node/MapNode.h"
#include "Future.h"


class GridGenerationResult;
class MovementAlgorithmTask;
class NodePathfindingStatus;

/**
 * 
 */
class MovementAlgorithmTask
{
public:
	AMapNode * NodeFrom;
	AArmy * ArmyToTestMovement;

	MovementAlgorithmTask(AMapNode * From, AArmy * ArmyToTest);

	virtual ~MovementAlgorithmTask();

	GridGenerationResult * GenerateGrid(float Movement);
	TFuture<GridGenerationResult*> GenerateGridAsync(float Movement);
	TFuture<GridGenerationResult*> GenerateNoLimitGridAsync();

	GridGenerationResult * Result;

private:
	GridGenerationResult * InnerGenerateGrid(float Movement);
	GridGenerationResult * InnerGenerateNoLimitGrid();

	bool IsFaster(NodePathfindingStatus* Start, NodePathfindingStatus* End, float movement);
	NodePathfindingStatus * MovementAlgorithmTask::TryNode(NodePathfindingStatus* Start, NodePathfindingStatus* End, float movement);
	NodePathfindingStatus * GetStatusForNode(AMapNode * NodeToCheck);
};

class NodePathfindingStatus
{
public:
	NodePathfindingStatus(AMapNode * NodeRepresenting);
	virtual ~NodePathfindingStatus();

	AMapNode * Node;
	AMapNode * PrevNode;
	float MoveLeft;
	ENodeStatus NodeStatus;
	bool IsVisited;
};

class GridGenerationResult
{
public:
	GridGenerationResult();
	virtual ~GridGenerationResult();

	TArray<AMapNode *> GenerateRoute(AMapNode * From, AMapNode * To);
	TArray<NodePathfindingStatus *> GenerateRouteData(AMapNode * From, AMapNode * To);
	TArray<NodePathfindingStatus*> Results;

private:
	
};