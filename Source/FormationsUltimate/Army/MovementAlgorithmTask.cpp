// Fill out your copyright notice in the Description page of Project Settings.

#include "FormationsUltimate.h"
#include "MovementAlgorithmTask.h"

//MovementAlgorithmTask
MovementAlgorithmTask::MovementAlgorithmTask(AMapNode * From, AArmy * ArmyToTest)
	: NodeFrom(From), ArmyToTestMovement(ArmyToTest)
{

}

MovementAlgorithmTask::~MovementAlgorithmTask()
{

}

GridGenerationResult* MovementAlgorithmTask::GenerateGrid(float Movement)
{
	return InnerGenerateGrid(Movement);
}

TFuture<GridGenerationResult*> MovementAlgorithmTask::GenerateGridAsync(float Movement)
{
	auto Result = Async<GridGenerationResult*>(EAsyncExecution::TaskGraph, [&]() {
		return InnerGenerateGrid(Movement);
	});
	return Result;
}

TFuture<GridGenerationResult*> MovementAlgorithmTask::GenerateNoLimitGridAsync()
{
	auto Result = Async<GridGenerationResult*>(EAsyncExecution::TaskGraph, [&]() {
		return InnerGenerateGrid(999999);
	});
	return Result;
}

GridGenerationResult * MovementAlgorithmTask::InnerGenerateGrid(float Movement)
{
	Result = new GridGenerationResult();
	float movement = Movement;
	AMapNode * start = NodeFrom;

	NodePathfindingStatus * StartStatus = new NodePathfindingStatus(start);

	Result->Results.Add(StartStatus);

	TArray<AMapNode*> NodesToTest;
	NodesToTest.Add(start);

	StartStatus->NodeStatus = ENodeStatus::NS_Passable;
	StartStatus->MoveLeft = movement;
	StartStatus->PrevNode = nullptr;
	StartStatus->IsVisited = true;

	while (NodesToTest.Num() > 0)
	{
		AMapNode* testedNode = NodesToTest[0];
		NodePathfindingStatus * TestedNodeStatus = GetStatusForNode(testedNode);
		for (int i = 0; i < testedNode->Neighbours.Num(); i++)
		{
			NodePathfindingStatus * NextNodeStatus = GetStatusForNode(testedNode->Neighbours[i]);
			NodePathfindingStatus * nodeToAddStatus = TryNode(TestedNodeStatus, NextNodeStatus, TestedNodeStatus->MoveLeft);
			if (nodeToAddStatus != nullptr)
			{
				if (!nodeToAddStatus->IsVisited)
				{
					Result->Results.Add(nodeToAddStatus);
					nodeToAddStatus->IsVisited = true;
					if(nodeToAddStatus->NodeStatus != ENodeStatus::NS_Attackable)
						NodesToTest.Add(nodeToAddStatus->Node);
				}
				
			}
		}
		NodesToTest.RemoveAt(0);
	}

	return Result;
}

GridGenerationResult * MovementAlgorithmTask::InnerGenerateNoLimitGrid()
{
	return nullptr;
}

bool MovementAlgorithmTask::IsFaster(NodePathfindingStatus* Start, NodePathfindingStatus* End, float movement)
{
	return (End->Node->GetCost(ArmyToTestMovement) <= movement) && (End->MoveLeft < (movement - End->Node->GetCost(ArmyToTestMovement)));
}

NodePathfindingStatus * MovementAlgorithmTask::TryNode(NodePathfindingStatus* Start, NodePathfindingStatus* End, float movement)
{
	NodePathfindingStatus * FoundMap = nullptr;
	bool foundBetter = false;

	if (!IsFaster(Start, End, movement)) {
		return nullptr;
	}
	if (End->Node->IsAttackable(ArmyToTestMovement)) {
		End->NodeStatus = ENodeStatus::NS_Attackable;
		FoundMap = End;
	}
	else if (End->Node->IsEnterable(ArmyToTestMovement)) {
		End->NodeStatus = ENodeStatus::NS_Enterable;
		FoundMap = End;
	}
	else if (End->Node->IsPassable(ArmyToTestMovement)) {
		End->NodeStatus = ENodeStatus::NS_Passable;
		FoundMap = End;
	}
	if (IsFaster(Start, End, movement)) {
		End->MoveLeft = movement - End->Node->GetCost(ArmyToTestMovement);
		End->PrevNode = Start->Node;
	}
	if (End->IsVisited) {
		return nullptr;
	}
	return FoundMap;
}

NodePathfindingStatus * MovementAlgorithmTask::GetStatusForNode(AMapNode * NodeToCheck)
{
	NodePathfindingStatus * StatusToReturn = nullptr;
	NodePathfindingStatus ** FoundStatus = nullptr;

	FoundStatus = Result->Results.FindByPredicate([&](NodePathfindingStatus * Status) {
		return Status->Node == NodeToCheck;
	});

	if (FoundStatus == nullptr) {
		StatusToReturn = new NodePathfindingStatus(NodeToCheck);
	}
	else {
		StatusToReturn = *FoundStatus;
	}
	return StatusToReturn;
}

//NodePathfindingStatus
NodePathfindingStatus::NodePathfindingStatus(AMapNode * NodeRepresenting)
	: MoveLeft(-1), PrevNode(nullptr), NodeStatus(ENodeStatus::NS_Nothing), IsVisited(false), Node(NodeRepresenting)
{
	
}

NodePathfindingStatus::~NodePathfindingStatus()
{

}

//GridGenerationResult
GridGenerationResult::GridGenerationResult()
{

}

GridGenerationResult::~GridGenerationResult()
{
	for (int i = 0; i < Results.Num(); i++) {
		NodePathfindingStatus* Status = Results[i];
		delete Status;
	}
}

TArray<AMapNode *> GridGenerationResult::GenerateRoute(AMapNode * From, AMapNode * To)
{
	TArray<AMapNode *> Route;
	return Route;
}

TArray<NodePathfindingStatus *> GridGenerationResult::GenerateRouteData(AMapNode * From, AMapNode * To) {
	TArray<NodePathfindingStatus *> Route;
	return Route;
}