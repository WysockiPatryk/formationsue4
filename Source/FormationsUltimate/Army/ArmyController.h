// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Controller.h"
#include "ArmyController.generated.h"

class AArmy;
class AMapNode;

/**
 * 
 */
UCLASS()
class FORMATIONSULTIMATE_API AArmyController : public AController
{
	GENERATED_BODY()

	AArmyController();

	AArmy * ControlledArmy;

	
	
	virtual void Possess(APawn * InPawn);
protected:
	void SetEvents();
	void GenerateGrid(AArmy * Army);
	void GoToNode(AMapNode * Node);
};
