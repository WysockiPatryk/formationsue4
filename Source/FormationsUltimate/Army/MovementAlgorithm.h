// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "MovementAlgorithm.generated.h"

class AArmy;
class AMapNode;

UCLASS( ClassGroup=(Custom))
class FORMATIONSULTIMATE_API UMovementAlgorithm : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMovementAlgorithm();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

public:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Astar")
		int32 moveLeft;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Astar")
		int32 maxMove;

	AArmy* owner;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Basic")
		TArray<AMapNode*> Route;

	UFUNCTION(BlueprintCallable, Category = "Astar")
		void GenerateGrid(AArmy * army);
	UFUNCTION(BlueprintCallable, Category = "Astar")
		void RemoveGrid(AArmy * army);
	UFUNCTION(BlueprintCallable, Category = "Astar")
		TArray<AMapNode*> GenerateRoute(AMapNode* start, AMapNode* end);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Astar")
		AMapNode* Location;

protected:
	AMapNode* TryNode(AMapNode* Start, AMapNode* End, float movement);
	bool IsFaster(AMapNode* Start, AMapNode* End, float movement);
	
};
