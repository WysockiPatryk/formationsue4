// Fill out your copyright notice in the Description page of Project Settings.

#include "FormationsUltimate.h"
#include "Army.h"
#include "Node/MapNode.h"
#include "Unit/CodeUnit.h"
#include "Army/MovementAlgorithm.h"
#include "Unit/MovementModifier/MovementModifier.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Leader/Leader.h"
#include "Combat/Combat.h"


// Sets default values
AArmy::AArmy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	IsSelectable = true;
	Algorithm = CreateDefaultSubobject<UMovementAlgorithm>("MovementAlgorithm");
	Algorithm->owner = this;
}

// Called when the game starts or when spawned
void AArmy::BeginPlay()
{
	CanAttack = true;
	OnSelect.AddDynamic(this, &AArmy::OnSelectGenerateGrid);
	OnDeselect.AddDynamic(this, &AArmy::OnDeselectRemoveGrid);
	OnOwnershipGain.AddDynamic(this, &AArmy::OnGainLeaderChangeColor);

	AGameMode *mode = GetWorld()->GetAuthGameMode();
	AGameModeFormationsUltimate * gameMode = Cast<AGameModeFormationsUltimate>(mode);
	if (gameMode) {
		gameMode->OnGameStateChangeDelegate.AddDynamic(this, &AArmy::ReactToChangeState);
		gameMode->OnGameStateChangeDelegate.AddDynamic(this, &AArmy::CheckForDeath);
		gameMode->OnGameStateChangeDelegate.AddDynamic(this, &AArmy::UpdateZoneOfControl);
		gameMode->OnGameStateChangeDelegate.AddDynamic(this, &AArmy::UpdateMovement);
		gameMode->FOnNextTurn.AddDynamic(this, &AArmy::ReactToTurnChange);
	}
	for (int i = 0; i < Units.Num(); i++) {
		UCodeUnit * Unit = Units[i];
		Unit->AddToOwner(this);
		OnUnitOwnershipGain.Broadcast(Unit);
		for (int i = 0; i < Unit->MovementModifiers.Num(); i++) {
			UMovementModifier * modifier = Unit->MovementModifiers[i];
			MaxMovement += modifier->FlatArmyMovementChange;
		}
	}
	Movement = MaxMovement;
	if (Position) {
		FVector newPos(Position->GetActorLocation().X, Position->GetActorLocation().Y, GetActorLocation().Z);
		SetActorLocation(newPos);
		Position->Stay(this);
	}
	OnMaxMovementChange.Broadcast(MaxMovement);
	OnMovementLeftChange.Broadcast(Movement);
	Super::BeginPlay();
	SubscribeToLeader();
	if (Position) {
		Position->CheckZoneOfControl(this);
		Position->CheckMovementRange(this);
	}
}

// Called every frame
void AArmy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AArmy::OnConstruction(const FTransform& Transform)
{
	FindNode(Transform);
}

AMapNode * AArmy::GetPosition()
{
	return this->Position;
}

bool AArmy::Select()
{
	if (IsSelectable) {
		OnSelect.Broadcast(this);
		return true;
	}
	else {
		return false;
	}
}

void AArmy::Deselect()
{
	OnDeselect.Broadcast(this);
}

float AArmy::GetMovement()
{
	return Movement;
}

void AArmy::OnSelectGenerateGrid(AArmy * SelectedArmy)
{
	Algorithm->GenerateGrid(SelectedArmy);
}

void AArmy::OnDeselectRemoveGrid(AArmy * SelectedArmy)
{
	Algorithm->RemoveGrid(SelectedArmy);
}

void AArmy::ReactToChangeState(EMainGameState newState)
{
	if (newState == EMainGameState::MGS_Strategy || newState == EMainGameState::MGS_UnitSelected) {
		IsSelectable = true;
	}
	else {
		IsSelectable = false;
	}
}

void AArmy::FindNode(const FTransform& Transform)
{
	float Offset = 50.0f;

	FVector Start;
	Start = GetActorLocation();

	FVector End;

	float Radius = 10.0f;
	float HalfHeight = 50.0f;

	TArray<TEnumAsByte<enum EObjectTypeQuery> > arrayTraceTypeAsByte;
	arrayTraceTypeAsByte.Add(EObjectTypeQuery::ObjectTypeQuery7);

	bool traceComplex = false;

	TArray<AActor*> IgnoredActors;

	EDrawDebugTrace::Type debugType = EDrawDebugTrace::None;

	TArray <FHitResult> hitResults;

	bool ignoreSelf = true;

	End = FVector(Start.X, Start.Y, Start.Z - Offset);
	hitResults.Empty();

	UKismetSystemLibrary::CapsuleTraceMultiForObjects(
		GetWorld(),
		Start,
		End,
		Radius,
		HalfHeight,
		arrayTraceTypeAsByte,
		traceComplex,
		IgnoredActors,
		debugType,
		hitResults,
		ignoreSelf
		);
	for (int i = 0; i < hitResults.Num(); i++)
	{
		AMapNode* hit = Cast<AMapNode>(hitResults[i].GetActor());
		if (hit)
		{
			Position = hit;
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Didnt found a node"));
		}
	}
}

void AArmy::SubscribeToLeader()
{
	AGameModeFormationsUltimate * gameMode = (AGameModeFormationsUltimate*)GetWorld()->GetAuthGameMode();
	ULeader * leader = gameMode->GetLeader(PlayerOwner);
	if (leader) {
		LeaderOwner = leader;
		leader->AddArmy(this);
		OnOwnershipGain.Broadcast(LeaderOwner);
		GameMode = gameMode;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Didnt found a leader"));
	}
}

void AArmy::UnsubscribeFromLeader()
{
	AGameModeFormationsUltimate * gameMode = (AGameModeFormationsUltimate*)GetWorld()->GetAuthGameMode();
	ULeader * leader = gameMode->GetLeader(PlayerOwner);
	if (leader) {
		LeaderOwner = leader;
		leader->RemoveArmy(this);
		OnOwnershipLose.Broadcast(LeaderOwner);
		GameMode = gameMode;
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Didnt found a leader"));
	}
}

void AArmy::MoveTo_Implementation(const TArray<AMapNode*> &route)
{
	RemoveZocAndMovementRanges();
}

void AArmy::RemoveZocAndMovementRanges()
{
	RemoveZocRange();
	RemoveMovementRange();
}

void AArmy::RemoveZocRange()
{
	TArray<AMapNode *> CopiedRangeArray = NodesInZoneOfControl;
	for (AMapNode * node : CopiedRangeArray) {
		node->GetOutFromZoneOfControlOf(this);
	}
}

void AArmy::RemoveMovementRange()
{
	TArray<AMapNode *> CopiedArray = NodesInRange;
	for (AMapNode * node : CopiedArray) {
		node->GetOutOfMovementRangeOf(this);
	}
}

void AArmy::PayMovement(float movementToPay) {
	Movement -= movementToPay;
	if (Movement < 0) {
		Movement = 0;
	}
	OnMovementLeftChange.Broadcast(Movement);
}

void AArmy::OnGainLeaderChangeColor(ULeader * NewLeader)
{

}

void AArmy::AddUnit(UCodeUnit * UnitToAdd)
{
	Units.Add(UnitToAdd);
	OnUnitOwnershipGain.Broadcast(UnitToAdd);
}

void AArmy::RemoveUnit(UCodeUnit * UnitToRemove)
{
	Units.Remove(UnitToRemove);
	OnUnitOwnershipLose.Broadcast(UnitToRemove);
}

void AArmy::AddUnitToArray(UCodeUnit * UnitToAdd)
{
	
}

void AArmy::RemoveUnitFromArray(UCodeUnit * UnitToRemove)
{
	Units.Remove(UnitToRemove);
}

void AArmy::AttackTo(const TArray<AMapNode*> &route, AArmy * ArmyToAttack)
{
	MoveTo(route);
	CanAttack = false;
	GetGameMode()->GetCombat()->BeginCombat(this, ArmyToAttack);
}

AGameModeFormationsUltimate * AArmy::GetGameMode()
{
	if (!GameMode) {
		GameMode = (AGameModeFormationsUltimate*)GetWorld()->GetAuthGameMode();
	}
	return GameMode;
}

void AArmy::ReactToTurnChange(ULeader * CurrentLeader, int32 TurnNumber, int32 TotalTurnNumber)
{
	if (CurrentLeader == LeaderOwner) {
		IsActive = true;
		CanAttack = true;
	}
	else
	{
		IsActive = false;
	}
	SetMovement(MaxMovement);
	RemoveZocRange();
	Position->CheckZoneOfControl(this);
	RemoveMovementRange();
	Position->CheckMovementRange(this);
	OnTurnChange.Broadcast(this, IsActive);
}

int32 AArmy::GetGlory()
{
	int32 retValue = 0;

	for (auto& unit : Units) {
		retValue += unit->Glory;
	}

	return retValue;
}

void AArmy::SetMaxMovementSpeed(int maxMovSpeed)
{
	MaxMovement = maxMovSpeed;
	OnMaxMovementChange.Broadcast(MaxMovement);
}

void AArmy::SetMovement(int movementToSet)
{
	Movement = movementToSet;
	OnMovementLeftChange.Broadcast(Movement);
}

TArray<UMovementModifier *> AArmy::GetMovementModifiers()
{
	TArray<UMovementModifier *> modifiersToReturn;
	for (UCodeUnit* Unit : Units)
	{
		modifiersToReturn.Append(Unit->MovementModifiers);
	}
	return modifiersToReturn;
}

void AArmy::Die()
{
	if (Position) {
		RemoveZocAndMovementRanges();
		Position->ArmyOnNode = nullptr;
	}
	if (LeaderOwner) {
		UnsubscribeFromLeader();
	}
	AGameMode *mode = GetWorld()->GetAuthGameMode();
	AGameModeFormationsUltimate * gameMode = Cast<AGameModeFormationsUltimate>(mode);
	if (gameMode) {
		gameMode->OnGameStateChangeDelegate.RemoveDynamic(this, &AArmy::ReactToChangeState);
		gameMode->OnGameStateChangeDelegate.RemoveDynamic(this, &AArmy::CheckForDeath);
		gameMode->OnGameStateChangeDelegate.RemoveDynamic(this, &AArmy::UpdateZoneOfControl);
		gameMode->OnGameStateChangeDelegate.RemoveDynamic(this, &AArmy::UpdateMovement);
		gameMode->FOnNextTurn.RemoveDynamic(this, &AArmy::ReactToTurnChange);
	}
	Destroy();
}


void AArmy::UpdateMovement(EMainGameState newState)
{
	if (newState == EMainGameState::MGS_Strategy)
	{
		RemoveMovementRange();
		Position->CheckMovementRange(this);
	}
}

void AArmy::UpdateZoneOfControl(EMainGameState newState)
{
	if (newState == EMainGameState::MGS_Strategy)
	{
		RemoveZocRange();
		Position->CheckZoneOfControl(this);
	}
}

void AArmy::CheckForDeath(EMainGameState newState)
{
	if (newState == EMainGameState::MGS_Strategy)
	{
		IsDead();
	}
}

void AArmy::IsDead()
{
	if (Units.Num() <= 0) {
		Die();
	}
}