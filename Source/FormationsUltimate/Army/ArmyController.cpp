// Fill out your copyright notice in the Description page of Project Settings.

#include "FormationsUltimate.h"
#include "ArmyController.h"
#include "Army/Army.h"
#include "Army/MovementAlgorithm.h"


AArmyController::AArmyController()
{
	
}

void AArmyController::Possess(APawn * InPawn)
{
	AArmy * potentialArmy = Cast<AArmy>(InPawn);
	if (potentialArmy) {
		ControlledArmy = potentialArmy;
		UE_LOG(LogTemp, Warning, TEXT("Created controller"));
		SetEvents();
		ControlledArmy->ArmyController = this;
	}
}

void AArmyController::SetEvents()
{
	
}

void AArmyController::GenerateGrid(AArmy * Army)
{
	
}

void AArmyController::GoToNode(AMapNode * Node)
{

}
