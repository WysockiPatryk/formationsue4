// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GameMode/GameModeFormationsUltimate.h"
#include "GameMode/MainGameState.h"
#include <map>
#include "Army.generated.h"

class ULeader;
class AGameModeFormationsUltimate;
class AArmyController;
class AMapNode;
class UMovementAlgorithm;
class UMovementModifier;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSelect, AArmy *, Army);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSetTarget, AMapNode *, TargetNode);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnOwnershipChange, ULeader *, NewLeader);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUnitOwnershipChange, UCodeUnit *, Unit);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTurnChange, AArmy *, Army, bool, IsActive);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMovementChanges, float, Movement);

UCLASS()
class FORMATIONSULTIMATE_API AArmy : public APawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AArmy();
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual void OnConstruction(const FTransform& Transform) override;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Army")
		AMapNode * Position;

	AMapNode * GetPosition();
	UPROPERTY(EditAnywhere, Category = "Army")
		int8 PlayerOwner;
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Army")
		ULeader * LeaderOwner;

	UPROPERTY(BlueprintReadOnly, Category = "Controller")
		AArmyController * ArmyController;

	UMovementAlgorithm * Algorithm;

	UFUNCTION()
		void ReactToChangeState(EMainGameState newState);

	UPROPERTY(BlueprintReadOnly, Category = "Selection")
		bool IsSelectable;

	UPROPERTY(BlueprintAssignable, Category = "Selection")
		FOnSelect OnSelect;
	UPROPERTY(BlueprintAssignable, Category = "Selection")
		FOnSelect OnDeselect;
	UFUNCTION(BlueprintCallable, Category = "Selection")
		bool Select();
	UFUNCTION(BlueprintCallable, Category = "Selection")
		void Deselect();

	UFUNCTION(BlueprintCallable, Category = "Selection")
		void Die();

	UFUNCTION(BlueprintCallable, Category = "Movement")
		void PayMovement(float movementToPay);
	UPROPERTY(BlueprintAssignable, Category = "Movement")
		FOnMovementChanges OnMaxMovementChange;
	UPROPERTY(BlueprintAssignable, Category = "Movement")
		FOnMovementChanges OnMovementLeftChange;

	float GetMovement();

	UFUNCTION()
		void OnSelectGenerateGrid(AArmy * SelectedArmy);
	UFUNCTION()
		void OnDeselectRemoveGrid(AArmy * SelectedArmy);
	UPROPERTY(BlueprintAssignable, Category = "Movement")
		FOnSetTarget OnSetTarget;

	UFUNCTION(BlueprintNativeEvent, Category = "Movement")
		void MoveTo(const TArray<AMapNode*> &route);
	
	void AttackTo(const TArray<AMapNode*> &route, AArmy * ArmyToAttack);
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Army")
		bool CanAttack;

	UPROPERTY(BlueprintAssignable, Category = "Ownership")
		FOnOwnershipChange OnOwnershipGain;
	UPROPERTY(BlueprintAssignable, Category = "Ownership")
		FOnOwnershipChange OnOwnershipLose;

	UFUNCTION()
		void OnGainLeaderChangeColor(ULeader * NewLeader);

	//Units
	UPROPERTY(BlueprintAssignable, Category = "Units")
		FOnUnitOwnershipChange OnUnitOwnershipGain;
	UPROPERTY(BlueprintAssignable, Category = "Units")
		FOnUnitOwnershipChange OnUnitOwnershipLose;

	UFUNCTION(BlueprintCallable, Category = "Units")
		void AddUnit(UCodeUnit * UnitToAdd);
	UFUNCTION(BlueprintCallable, Category = "Units")
		void RemoveUnit(UCodeUnit * UnitToRemove);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Units")
		int32 GetGlory();

	UFUNCTION()
		void AddUnitToArray(UCodeUnit * UnitToAdd);
	UFUNCTION()
		void RemoveUnitFromArray(UCodeUnit * UnitToRemove);
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Instanced, Category = "Units")
		TArray<UCodeUnit *> Units;
	AGameModeFormationsUltimate * GetGameMode();

	UPROPERTY(BlueprintReadOnly, Category = "Turn")
		bool IsActive;
	UFUNCTION()
		void ReactToTurnChange(ULeader * CurrentLeader, int32 TurnNumber, int32 TotalTurnNumber);
	UPROPERTY(BlueprintAssignable, Category = "Turn")
		FOnTurnChange OnTurnChange;
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void SetMaxMovementSpeed(int maxMovSpeed);
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void SetMovement(int movementToSet);
	UFUNCTION(BlueprintCallable, Category = "Movement")
		TArray<UMovementModifier *> GetMovementModifiers();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "ZoneOfControl")
		TArray<AMapNode*> NodesInZoneOfControl;
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "ZoneOfControl")
		TArray<AMapNode*> NodesInRange;

	void RemoveZocAndMovementRanges();
	void RemoveZocRange();
	void RemoveMovementRange();

	void IsDead();

protected:
	AGameModeFormationsUltimate * GameMode;
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Movement")
		float Movement;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Movement")
		float MaxMovement;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Movement")
		float AfterBattleMovement;

	void FindNode(const FTransform& Transform);
	void SubscribeToLeader();
	void UnsubscribeFromLeader();

	UFUNCTION()
		void UpdateMovement(EMainGameState newState);
	UFUNCTION()
		void UpdateZoneOfControl(EMainGameState newState);
	UFUNCTION()
		void CheckForDeath(EMainGameState newState);
};
